import "./Link.css";

function Link(props) {
  let { text, link } = props;

  return (
    <a className="link" target="_blank" rel="noreferrer" href={link}>{text}</a>
  );
}

export default Link;
