import "./header.css";
import { Link } from "react-router-dom";
import { useSelector} from "react-redux";
import image from "./favicon.svg";
import imageHeart from "./heart_1.svg";
import imageCart from "./food-basket2.svg";

function Header(props) {
  // const cart = useSelector((store) => store.cart);
  const countCurt = useSelector((state) => state.cart.products.length);
  const countLike = useSelector((state) => state.cart.like.length);

  return (
    <>
      <header className="header">
        <div className="market-name container" id="top-anchor">
          <Link to="/">
            <div className="market-logo">
              <div className="market-line">
                <div className="line-login">
                  <img src={image} alt="Картинка" className="logo" />
                </div>
                <div className="line-login">
                  <div className="line">
                    <span className="orange">Мой</span><span className="blue">Маркет</span>
                  </div>
                </div>
              </div>
            </div>
          </Link>

          <div className="icons">
            <div className={`cart-number-1 ${countCurt === 0 ? "hidden" : ""}`}>
            {`${countCurt}`}
            </div>
            <div className={`cart-number-2 ${countLike === 0 ? "hidden" : ""}`}>
            {`${countLike}`}
            </div>
            <img className="cart-img-1" src={imageHeart} alt="like" />
            <img className="cart-img-2" src={imageCart} alt="cart" />
          </div>
        </div>
      </header>
    </>
  );
}

export default Header;
