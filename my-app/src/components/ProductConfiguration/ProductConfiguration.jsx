import { useState,} from "react";
import "./ProductConfiguration.css";
import ConfigurationButton from "./ConfigurationButton";



function ProductConfiguration(props) {
  const { buttons } = props
  const [idActive, setIdActive] = useState(localStorage.getItem('configChoice') || '128 ГБ');
  const handleClick = (text) => setIdActive(text);
  localStorage.setItem('configChoise', idActive);

  return (
    <div>
      <section className="product-configuration">
        <div className="product__subtitle change-memory">
          Конфигурация памяти: {' '} {idActive}
        </div>

        <div className="product__memory-buttons">
          {
            buttons.map((item) =>

              <div onClick={() => handleClick(item.text)} key={item.text}>
                <ConfigurationButton
                  item={item}
                  isActive={item.text === idActive}
                />
              </div>
            )
          }
        </div>
      </section>
    </div>
  );
}

export default ProductConfiguration;
