import { useState, useCallback } from "react";

import "./Main.css";
import ProductViews from "../ProductViews/ProductViews";
import ProductColor from "../ProductColor/ProductColor";
import ProductConfiguration from "../ProductConfiguration/ProductConfiguration";
import ProductCharacteristics from "../ProductCharacteristics/ProductCharacteristics";
import ProductDescription from "../ProductDescription/ProductDescription";
import ProductComparison from "../ProductComparison/ProductComparison";
import Reviews from "../Reviews/Reviews";
import Form from "../Form/Form";
import Sidebar from "../Sidebar/Sidebar";
// import MessageCart from "../MessageCart/MessageCart";

let characteristicitem = [
  {
    text: "Apple A15 Bionic",
    link: "https://ru.wikipedia.org/wiki/Apple_A15",
  },
];

let colorButtons = [
  {
    src: "./color-1.png",
    className: "product__image",
    id: 1,
  },
  {
    src: "./color-2.png",
    className: "product__image",
    id: 2,
  },
  {
    src: "./color-3.png",
    className: "product__image",
    id: 3,
  },
  {
    src: "./color-4.png",
    className: "product__image",
    id: 4,
  },
  {
    src: "./color-5.png",
    className: "product__image",
    id: 5,
  },
  {
    src: "./color-6.png",
    className: "product__image",
    id: 6,
  },
];

let configurationButtons = [
  {
    text: "128 ГБ",
    className: "product__button",
    id: 1,
  },
  {
    text: "256 ГБ",
    className: "product__button",
    id: 2,
  },
  {
    text: "512 ГБ",
    className: "product__button",
    id: 3,
  },
];

function Main(props) {
  const [colorActive, setColorActive] = useState(
    localStorage.getItem("colorChoice")
  );
  const handleClickColor = useCallback((color) => setColorActive(color), []);

  // применение хука  useCallback не рационально, 
  // в нашем проекте думаю он не нужен,
  // здесь для примера показана его реализация

  localStorage.setItem("colorChoise", colorActive);

  return (
    <div className="main">
      <ProductViews />

      <div className="product-container">
        <div className="product-information">
          <ProductColor
            buttons={colorButtons}
            handleClickColor={handleClickColor}
            colorActive={colorActive}
          />
          <ProductConfiguration buttons={configurationButtons} />
          <ProductCharacteristics item={characteristicitem} />
          <ProductDescription />
          <div className="product__comparison">
            <div className="product__subtitle">Сравнение моделей</div>
            <ProductComparison />
          </div>

          <section className="reviews-container">
            <div className="reviews__info">
              <div className="reviews__box-wrapped">
                <h2 className="reviews__feedback">
                  Отзывы<span className="gray">425</span>
                </h2>
              </div>
            </div>

            <Reviews />

            <Form />
          </section>
        </div>

        <Sidebar
        // handleClickButton={handleClickButton}
        // ActiveButton={ActiveButton}
        // handleClickLike={handleClickLike}
        // ActiveLike={ActiveLike}
        />
      </div>
    </div>
  );
}

export default Main;
