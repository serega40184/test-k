import React from "react";
import Header from "../Header/Header";
import Footer from "../Footer/Footer";
import styled from "styled-components"
import productData from "../Data";


const ErrorContent = styled.div`
display: flex;
flex-direction: column;
justify-content: center;
align-items: center;
padding: 0px;
gap: 20px;
height: 475px;
width: 1400px;
margin: auto;
@media (min-width: 360px) and (max-width: 1023px) {
width: 360px;
margin: auto;}`

const ErrorText = styled.div`
width: 453px;
height: 57px;
font-size: 200%;
font-weight: 600;
line-height: 19px;
text-align: center;
color: black;
@media (min-width: 360px) and (max-width: 1023px) {
  width: 340px;
  font-size: 150%;
}`

function PageNotFound() {
  return (
    <main >
      <Header />
      <ErrorContent>
        <ErrorText>NOT FOUND</ErrorText>
      </ErrorContent>
      <Footer list={productData.items} />
    </main>
  );
}

export default PageNotFound;
