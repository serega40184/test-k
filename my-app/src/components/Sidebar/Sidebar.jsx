import { useSelector, useDispatch } from "react-redux";
import { addProduct, dellProduct,addLike, delllike } from "../../cart-reducer";
import "./Sidebar.css";
import Iframe from "../Iframe/Iframe";
import MessageCart from "../MessageCart/MessageCart";
import { useState } from "react";

function Sidebar() {
  const count = useSelector((state) => state.cart.products);

  const cart = useSelector((store) => store.cart);
  const dispatch = useDispatch();
  const [clicks, setClicks] = useState(0)
  const SetInCart = count.some((prevProduct) => { return prevProduct.id === 777777 })
  const count1 = useSelector((state) => state.cart.like);
  const SetInLike = count1.some((prevLike) => { return prevLike.id === 16891689 })


  function handleClickAddLike() {
    if (SetInLike) {
      const action = delllike({ id: 16891689 })
      dispatch(action);
    }
    else {
      const action = addLike({ id: 16891689 });
      dispatch(action);
    }
  }
  function handleClickAdd() {
    if (SetInCart) {
      const action = dellProduct({ id: 777777 })
      dispatch(action);
    }
    else {
      const action = addProduct({ id: 777777 });
      dispatch(action);
      setClicks((prevState) => prevState + 1);
    }
  }


  return (
    <div className="product__sidebar">
      <div className="product__price price">
        <div className="price__group">
          <div className="price__discount">
            <s className="gray-1">
              <b>75 990₽</b>
            </s>
            <div className="price__percent-wrapped">
              <p className="price__percent">-8%</p>
            </div>
            <div
              onClick={handleClickAddLike}
              className={`sidebar-info__like ${
                SetInLike ? "like_choise" : ""
              }`}
            ></div>
          </div>
          <div className="price__sum">67 990₽</div>
        </div>
        <div className="price__comment">
          <p>
            Самовывоз в четверг, 1 сентября — <b>бесплатно</b>
          </p>
          <p>
            Курьером в четверг, 1 сентября — <b>бесплатно</b>
          </p>
        </div>
        <button
          onClick={handleClickAdd}
          className={` ${SetInCart  ? 'choise' : 'sidebar__button'}`} 
        >
         {` ${SetInCart  ? 'Убрать из корзины' : 'Добавить в корзину'}`}  
        </button>
      </div>

      <div className="advertisement">
        <p>Реклама</p>
        <div className="advertisement__iframes">
          <div className="ads">
            <Iframe />
          </div>
          <div className="ads">
            <Iframe />
          </div>
        </div>
      </div>
      <MessageCart clicks={clicks} />
    </div>
  );
}

export default Sidebar;